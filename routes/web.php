<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DingController@index');
Route::get('/ding_list/{order_token}', 'DingController@index');
Route::get('/order_list/{token}', 'DingController@orderList');
Route::get('/my_order/{token}', 'DingController@myOrder');

Route::post('/ding', 'DingController@ding');
Route::post('/ding_order', 'DingController@dingOrder');
Route::post('/ding_list/{order_token}', 'DingController@dingList');
Route::post('/haspay', 'DingController@hasPay');

Route::delete('/ding_del', 'DingController@dingDelete');