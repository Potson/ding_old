<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ding Lunch !</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ url('/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/style.css') }}">

        <script type="text/javascript" src="{{ url('/js/common.js') }}" defer></script>
    </head>
    <body>
        <div id="App">
            <div class="container">
                <div class="row">
                    <div id="wait" class="col-xs-12 text-center"><span class="fa fa-refresh fa-spin fa-3x fa-fw"></span></div>
                </div>
            </div>
        </div>
    </body>

    @include('react')
</html>
