<script src="{{ url('/babel/react/15.4.2/react.min.js') }}" defer></script>
<script src="{{ url('/babel/react-dom/15.4.2/react-dom.min.js') }}" defer></script>
<script src="{{ url('/babel/polyfill.min.js') }}" defer></script>
<script src="{{ url('/babel/babel.min.js') }}" defer></script>
<script src="{{ url('/babel/es6-promise.auto.min.js') }}" defer></script>
<script src="{{ url('/babel/axios/0.16.1/axios.min.js') }}" defer></script>
<script type="text/babel" src="{{ url('/js/DingApp.js') }}" defer></script>
<script type="text/babel">
    ReactDOM.render(
        <DingApp token="{{ Cookie::get('dingToken') }}" @if(isset($order)) order="{{ $order }}" @endif @if(isset($page)) page="{{ $page }}" @endif />,
        document.getElementById('App')
    );
</script>