<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOIdToDing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ding', function (Blueprint $table) {
            $table->unsignedBigInteger('o_id')->index()->nullable()->default(NULL)->comment('訂單 id');
            $table->foreign('o_id')->references('id')->on('order')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ding', function (Blueprint $table) {
            $table->dropForeign(['o_id']);
            $table->dropColumn('o_id');
        });
    }
}
