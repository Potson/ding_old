<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('subject')->comment('訂單名稱');
            $table->string('menu')->nullable()->default(NULL)->comment('訂單圖片');
            $table->string('token',32)->index()->comment('發起訂購人 token');
            $table->string('order_token',32)->index()->comment('訂單辨識 token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
