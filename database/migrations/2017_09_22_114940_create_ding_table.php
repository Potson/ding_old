<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ding', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('訂購人名稱');
            $table->string('content')->comment('訂購內容');
            $table->unsignedInteger('price')->comment('訂購價格');
            $table->string('token',32)->nullable()->default(NULL)->index()->comment('辨認訂購人 token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ding');
    }
}
