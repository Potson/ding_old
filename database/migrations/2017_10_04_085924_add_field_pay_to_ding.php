<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPayToDing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ding', function (Blueprint $table) {
            $table->boolean('pay')->nullable()->default(NULL)->comment('已付款記錄; NULL => 無資料, 0 => 尚未付款 (訂單經變更), 1=> 已付款');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ding', function (Blueprint $table) {
            $table->dropColumn('pay');
        });
    }
}
