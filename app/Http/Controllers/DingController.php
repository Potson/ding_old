<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Ding;
use App\Order;
use QrCode;
use DB;


class DingController extends Controller
{
    public function index(Request $request, $order_token=false)
    {
        if(!empty($order_token)){
            $order = Order::where('order_token', $order_token)->first();
            $order->link = url('/ding_list/'.urlencode($order->order_token));
            $order->qrcode = $this->dingQrcode($order->order_token,$order->link);
        }

        $dingToken = $request->cookie('dingToken');

        if(empty($dingToken)) $dingToken = md5(rand().date(strtotime('now')));

        if(!empty($order_token)){
            return response()->view('ding', ['order' => $order, 'page' => 'form'])->withCookie(cookie()->forever('dingToken', $dingToken));
        }else{
            return response()->view('ding')->withCookie(cookie()->forever('dingToken', $dingToken));
        }
    }


    public function orderList($token)
    {
        if(!empty($token)){
            $model = Order::where('token', $token)->orderBy('created_at', 'desc')->get();

            if($model->isNotEmpty()){
                foreach($model as $itemKey => $item){
                    $model[$itemKey]->link = url('/ding_list/'.urlencode($item->order_token));
                    $model[$itemKey]->qrcode = $this->dingQrcode($item->order_token,$model[$itemKey]->link);
                }

                return response()->json(['success' => true, 'data' => $model]);
            }
        }

        return response()->json(['error' => 'No record.']);
    }


    public function myOrder($token)
    {
        if(!empty($token)){
            $model = Ding::where('token', $token)->whereNotNull('o_id')->where('o_id', '>', '0')->orderBy('created_at', 'desc')->get();

            if($model->isNotEmpty()){
                foreach($model as $item){
                    $o_idGroup[$item['o_id']] = true;
                }
            }

            if(isset($o_idGroup) && is_array($o_idGroup)){
                $o_ids = array_keys($o_idGroup);
                $order = Order::whereIn('id', $o_ids)->get();

                if($order->isNotEmpty()){
                    foreach($order as $orderItemKey => $orderItem){
                        $thisDing = Ding::where('o_id' , $orderItem->id)->orderBy('created_at', 'desc')->first();

                        $order[$orderItemKey]->link = url('/ding_list/'.urlencode($orderItem->order_token));
                        $order[$orderItemKey]->qrcode = $this->dingQrcode($orderItem->order_token,$order[$orderItemKey]->link);
                        $order[$orderItemKey]->content = $thisDing->content;
                        $order[$orderItemKey]->time = $thisDing->updated_at->format('Y/m/d H:i:s');
                    }

                    return response()->json(['success' => true, 'data' => $order]);
                }
            }
        }

        return response()->json(['error' => 'No record.']);
    }


    public function dingOrder(Request $request)
    {
        $messages = [
            'required'    => '缺少資訊',
            'boolean'     => '參數錯誤',
            'required_if' => '請選擇圖片檔案，或點選 "不需要訂單"',
            'file'        => '請選擇圖片檔案，或點選 "不需要訂單"',
            'image'       => '請選擇圖片檔案，或點選 "不需要訂單"'
        ];

        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'no_menu' => 'boolean',
            'menu'    => 'required_if:no_menu,0|file|image|max:5120'
        ], $messages);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()]);
        }

        $model = new Order;

        $model->subject     = $request->subject;
        $model->token       = $request->cookie('dingToken');
        $model->order_token = md5('order'.rand().date(strtotime('now')));

        if(isset($request->menu)){
            $menu = $request->file('menu')->store('public/menus');
            $model->menu = url(preg_replace('/^public\/(.*)/', '/storage/$1', $menu));
        }

        $model->save();

        $model->link = url('/ding_list/'.urlencode($model->order_token));
        $model->qrcode = $this->dingQrcode($model->order_token,$model->link);

        return response()->json(['success' => true, 'data' => $model]);
    }


    public function ding(Request $request)
    {
        $dingToken = $request->cookie('dingToken');

        $messages = [
            'required' => '缺少資訊',
            'numeric'  => '請輸入數字',
            'min'      => '真假！？免錢喔？'
        ];

        $validator = Validator::make($request->all(), [
            'order_token' => 'required',
            'name'        => 'required',
            'content'     => 'required',
            'price'       => 'required|numeric|min:1'
        ], $messages);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()]);
        }

        $newDing = false;
        $post    = $request->all();

        $orderToken = urldecode($post['order_token']);
        $order = Order::where('order_token', $orderToken)->first();

        if(empty($order)){
            return response()->json(['error' => '找不到訂單，如果重整後依然出現此訊息，請通知管理員！']);
        }

        if(!isset($post['id']) || empty($post['id'])){
            $model = Ding::where('token', $dingToken)->where('o_id', $order->id)->first();

            if(empty($model)){
                $model = new Ding;
                $newDing = true;
            }
        }else{
            $model = Ding::find($post['id']);
        }

        $model->name    = $post['name'];
        $model->content = $post['content'];
        $model->price   = $post['price'];
        $model->token   = $dingToken;
        $model->o_id    = $order->id;
        $model->pay     = false;

        $model->save();

        $successMsg = ($newDing) ? '加入訂購成功。' : '修改訂單成功';

        Cache::store('redis')->forget('dingList'.$order->id);

        return response()->json(['success' => $successMsg, 'data' => $model]);
    }


    public function dingList($order_token)
    {
        $order = Order::where('order_token', $order_token)->first();
        return Cache::store('redis')->get('dingList'.$order->id, $this->getAllDing($order_token));
    }


    public function dingQrcode($order_token,$link)
    {
        $fileName = $order_token.'.'.'svg';

        if(!Storage::disk('local')->exists('/public/qrcode/'.$fileName)){
            $qrcode = QrCode::encoding('UTF-8')->size('300')->margin('0')->backgroundColor(237,236,237)->generate($link);

            Storage::disk('local')->put('/public/qrcode/'.$fileName, $qrcode);
        }

        return Storage::disk('local')->url('qrcode/'.$fileName);
    }


    public function dingDelete(Request $request)
    {
        if(isset($request->token) && !empty($request->token)){

            Ding::where('token', $request->token)->where('o_id', $request->o_id)->delete();

            Cache::store('redis')->forget('dingList'.$request->o_id);

            $report = ['message' => '取消訂購成功！'];
        }else{
            $report = ['message' => '取消訂購！找不到訂購資訊！'];
        }

        return response()->json($report);
    }


    public function hasPay(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()]);
        }

        $model = Ding::find($request->id);

        if(!empty($model)){
            $model->pay = true;
            $model->save();

            Cache::store('redis')->forget('dingList'.$model->o_id);

            return response()->json(['success' => 'haspay saved.']);
        }else{
            return response()->json(['error' => 'No record.']);
        }
    }


    private function getAllDing($order_token)
    {
        $order = Order::where('order_token', $order_token)->first();
        $model = Ding::where('o_id', $order->id)->whereBetween('updated_at', [date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59'])->get();
        $jsonData = json_encode(['data' => $model]);

        Cache::store('redis')->forever('dingList'.$order->id, $jsonData);

        return $jsonData;
    }
}
