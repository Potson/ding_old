var isset = function(arg,callback){
    var result = false;
    if(typeof arg != 'undefined' && arg != '' && arg != 0 && arg != NaN && arg !== false) result = true;
    return (result && typeof callback == 'function')?callback(arg):result;
}

var log = function(output){
    try{
        console.log(output);
    }
    catch(e){
        alert(output);
    }
}
