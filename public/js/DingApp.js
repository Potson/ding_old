class DingApp extends React.Component {

    /**
     * Ding 前端
     * @Auther  PotsonHumer  potsonhumer@gmail.com
     *
     * @param String          this.props.token    訂購人辨識 token
     * @param Number          this.state.id       訂購項目 id
     * @param String          this.state.name     訂購人姓名
     * @param String          this.state.content  訂購內容
     * @param Number          this.state.price    訂購價格
     * @param Object|Boolean  this.state.data     訂單內容
     * @param String          this.state.page     內頁模式標記 index|order|form|history
     */

    constructor(props) {
        super(props);

        this.state = {
            menu      : {},
            noMenu    : false,
            subject   : '',
            id        : '',
            name      : '',
            content   : '',
            price     : 0,
            orderList : false,
            myOrder   : false,
            order     : (isset(this.props.order)) ? JSON.parse(decodeURIComponent(this.props.order)) : {},
            data      : false,
            page      : (isset(this.props.page)) ? this.props.page : 'index'
        };
    }

    isArray(args) {
        return (isset(args) && Array.isArray(args)) ? true : false;
    }

    isObject(args) {
        return (isset(args) && typeof(args) == 'object' && !Array.isArray(args)) ? true : false;
    }

    ajax(method,url,data,thenCallback,errCallback,config={}) {
        if(!isset(url)){
            alert('Ajax response no send path');
            return;
        }

        axios({
            method: (isset(method)) ? method : 'post',
            url: url,
            data: data
        },config).then((response) => {

            var data = response.data;

            switch(true){
                case (isset(data)):
                    if(typeof(thenCallback) == 'function') thenCallback(data);
                break;
                default:
                    alert('伺服器端無資料回應！ Server no response！');
                    if(typeof(errCallback) == 'function') errCallback();
                break;
            }            
        }).catch((err) => {
            if(typeof(errCallback) != 'function'){
                log(err);
            }else{
                if(typeof(errCallback) == 'function') errCallback(err);
            }
        });
    }

    intFormat(number)
    {
        return new Intl.NumberFormat('en-US').format(number)
    }


    dingPage()
    {
        switch(this.state.page){
            case 'index':
                return this.dingHome()
                break
            case 'order':
                return this.dingOrder()
                break
            case 'form':
                return this.dingForm()
                break
            case 'history':
                return this.myOrder()
                break
        }
    }


    dingHome()
    {
        return (
            <div style={{ paddingBottom: 200 }}>
                <div className="row">
                    <strong className="col-xs-12">什麼是 Ding！？</strong>
                    <p className="col-xs-12">訂便當、飲料、團購 ...，不用註冊、不用登入，輕鬆貼寫就開始！</p>
                    <p className="col-xs-12">
                        <button className="btn btn-info" style={{ marginTop: '1em' }} onClick={() => this.setState({page: 'order'})}>現在就開始 Ding！</button>
                    </p>
                </div>

                <hr style={{ borderColor: '#CCC' }} />

                { this.orderList() }
            </div>
        )
    }


    myOrder()
    {
        var myOrder = this.state.myOrder

        switch(true){
            case (myOrder === false):
                return (
                    <div className="row">
                        <div className="col-xs-12 text-center">
                            <span className="fa fa-refresh fa-spin fa-3x fa-fw"></span>
                        </div>
                    </div>
                )
                break

            case (myOrder.length > 0):
                var orderListElement = myOrder.map((order,index) => {
                    return (
                        <li className="clear flex flex-middle flex-wrap">
                            <span className="col-xs-12 col-sm-3 col-md-3">名稱：{order.subject}</span>
                            <span className="col-xs-12 col-sm-4 col-md-3">訂購內容：{order.content}</span>
                            <span className="col-xs-12 col-sm-5 col-md-4">訂購時間：{order.time}</span>
                            <span className="col-xs-12 col-md-2 text-right"><a className="btn btn-default" onClick={() => this.pushState([{foo: 'order'},'',order.link], this.setState({page: 'form', order: order}))}><i className="fa fa-hand-pointer-o"></i> 前往</a></span>
                        </li>
                    )
                })

                return (
                    <div className="row">
                        <ul id="orderList" className="clear" style={{ paddingTop: 0 }}>
                            <li className="clear">
                                <strong className="col-xs-12">我參與過的 Ding！</strong>
                            </li>
                            { orderListElement }
                        </ul>
                    </div>
                )
                break

            default:
                return (
                    <div className="row" style={{ paddingBottom: 150 }}>
                        <h3 className="col-xs-12 text-center" style={{ cursor: 'pointer' }} onClick={() => this.setState({page: 'order'})}>
                            無訂單記錄！
                            <small>現在就開始 Ding ~~</small>
                        </h3>
                    </div>
                )
                break
        }
    }


    orderList()
    {
        var orderList = this.state.orderList

        switch(true){
            case (orderList === false):
                return (
                    <div className="row">
                        <div className="col-xs-12 text-center">
                            <span className="fa fa-refresh fa-spin fa-3x fa-fw"></span>
                        </div>
                    </div>
                )
                break

            case (orderList.length > 0):
                var orderListElement = orderList.map((order,index) => {
                    return (
                        <li className="clear flex flex-middle flex-wrap">
                            <span className="col-xs-12 col-sm-3 col-md-3">名稱：{order.subject}</span>
                            <span className="col-xs-12 col-sm-6 col-md-4">創建時間：{order.created_at}</span>
                            <span className="col-xs-12 col-sm-3 col-md-2">保存菜單：{ (isset(order.menu) && order.menu !== null) ? '是' : '否' }</span>
                            <span className="col-xs-12 col-md-3 text-right"><a className="btn btn-default" onClick={() => this.pushState([{foo: 'order'},'',order.link], this.setState({page: 'form', order: order}))}><i className="fa fa-hand-pointer-o"></i> 前往</a></span>
                        </li>
                    )
                })

                return (
                    <div className="row">
                        <ul id="orderList" className="clear">
                            <li className="clear">
                                <strong className="col-xs-12">之前建立的 Ding！</strong>
                            </li>
                            { orderListElement }
                        </ul>
                    </div>
                )
                break

            default:
                return (
                    <div className="row" style={{ paddingBottom: 150 }}>
                        <h3 className="col-xs-12 text-center" style={{ cursor: 'pointer' }} onClick={() => this.setState({page: 'order'})}>
                            無歷史訂單！
                            <small>現在就開始 Ding ~~</small>
                        </h3>
                    </div>
                )
                break
        }
    }


    dingOrder()
    {
        return (
            <div id="dingOrder" className="row">
                <strong className="col-xs-12">新 Ding！</strong>
                <form>
                    <div className="form-group col-xs-12 col-md-6">
                        <label>上傳訂單：</label>
                        <div className="form-inline">
                            <input type="file" className="form-control" name="menu" accept="image/*" disabled={ (this.state.noMenu) ? true : false } onChange={(e) => this.setState({menu: e.target.files[0]})} />
                            <button type="button" className={`btn animate btn-warning ${(this.state.noMenu) ? 'active' : ''}`} onClick={() => this.setState({noMenu: !this.state.noMenu})}>不需要訂單</button>
                            <div className="help-block"><span className="text-info">限上傳圖片 (jpg, jpeg, png, gif)，最大檔案大小 5MB</span></div>
                        </div>
                    </div>
                    <div className="form-group col-xs-12 col-md-6">
                        <label>訂單名稱：</label>
                        <input type="text" className="form-control" name="subject" placeholder="例：訂雞排" value={this.state.subject} onChange={(e) => this.setState({subject: e.target.value})} />
                    </div>
                    <div className="col-xs-12 col-md-6">
                        <button type="button" className="btn btn-success" onClick={(e) => this.createOrder(e)}>開始訂購！</button>
                    </div>
                </form>
            </div>
        )
    }


    dingForm()
    {
        return (
            <div>
                <div className="row">
                    <div className="clear">
                        <strong className="col-xs-12">揪團眾</strong>
                        <p className="col-xs-12">分享網址或使用手機掃描 QR Code 邀請其他人來訂購。</p>

                        <div className="shareBlock col-xs-12 col-md-4">
                            <label>掃描 QR Code：</label>
                            <div>
                                <img src={this.state.order.qrcode} className="img-responsive" style={{ width: 150 }} />
                            </div>
                        </div>

                        <div className="shareBlock col-xs-12 col-md-8">
                            <label>複製網址：</label>
                            <div className="input-group">
                                <input type="text" id="link" className="form-control" value={this.state.order.link} />
                                <span className="input-group-btn">
                                    <button type="button" className="btn btn-info" onClick={() => this.copyUrl()} >複製網址</button>
                                </span>
                            </div>
                        </div>

                        <div className="shareBlock col-xs-12" style={{ display: (isset(this.state.order.menu) && this.state.order.menu != '' && !(typeof this.state.order.menu == 'object' && this.state.order.menu === null)) ? 'block' : 'none' }}>
                            <label>菜單：</label>
                            <div>
                                <img src={this.state.order.menu} className="img-responsive" />
                            </div>
                        </div>
                    </div>

                    <strong className="col-xs-12" style={{ marginTop: 30 }}>{ this.state.order.subject }</strong>
                    <form>
                        <div className="form-group col-xs-12 col-md-4">
                            <label>姓名：</label>
                            <input type="text" className="form-control" name="name" value={this.state.name} onChange={(e) => this.setState({name: e.target.value})} />
                        </div>
                        <div className="form-group col-xs-12 col-md-6">
                            <label>訂購內容：</label>
                            <input type="text" className="form-control" name="content" value={this.state.content} onChange={(e) => this.setState({content: e.target.value})} />
                        </div>
                        <div className="form-group col-xs-12 col-md-2">
                            <label>價格：</label>
                            <input type="number" className="form-control" name="price" min="1" value={this.state.price} onChange={(e) => this.setState({price: e.target.value})} />
                        </div>
                        <div className="col-xs-12 col-md-3">
                            <button type="button" className="btn btn-success" onClick={(e) => this.doDing(e)}>我 Ding！</button>
                        </div>
                        <input type="hidden" name="id" value={this.state.id} />
                    </form>
                </div>

                <hr style={{ borderColor: '#CCC' }} />

                { this.dingList() }

            </div>
        )
    }


    dingList()
    {
        var datas = this.state.data

        switch(true){
            case (datas === false):
                var dingList = (
                    <div className="row">
                        <div className="col-xs-12 text-center">
                            <span className="fa fa-refresh fa-spin fa-3x fa-fw"></span>
                        </div>
                    </div>
                )
                break

            case (this.isArray(datas)):

                var delBtn = (data) => {
                    return (
                        <div className="col-xs-12 col-sm-6 col-md-2">
                            { (data.token == this.props.token) ? <button type="button" className="btn btn-danger pull-right" style={{ margin: '0 0 10px 10px' }} onClick={() => this.dingDel(data.token)}>取消訂購</button> : false}
                            { (this.state.order.token == this.props.token) ? <button type="button" className={`btn btn-success pull-right ${ (isset(data.pay) && data.pay) ? 'active' : '' }`} style={{ marginBottom: 10 }} onClick={() =>  this.pay(data.id)}>已收款</button> : false }
                        </div>
                    )
                }

                var dingList = datas.map((data,index) => {
                    return (
                        <div className="clear" style={{ color: (isset(data.pay) && data.pay) ? '#AAA' : '#000' }} >
                            <div className="dingList flex flex-wrap flex-middle clear">
                                <div className="col-xs-12 col-md-2">姓名：{data.name}</div>
                                <div className="col-xs-12 col-md-6">訂購內容：{data.content}</div>
                                <div className="col-xs-12 col-sm-6 col-md-2">價格：{this.intFormat(data.price)}</div>

                                { delBtn(data) }

                            </div>

                            <hr style={{ borderColor: '#CCC', marginLeft: 15, marginRight: 15 }} />
                        </div>
                    )
                })
                break

            default:

                var dingList = (
                    <div className="clear" style={{ paddingBottom: 200 }}>
                        <h3 className="col-xs-12 text-center">
                            尚無訂單！
                            <small>現在就開始 Ding ~~</small>
                        </h3>
                    </div>
                )
                break
        }

        return (
            <div className="row">
                { dingList }
                { this.showSum() }
            </div>
        )
    }


    showSum()
    {
        var datas = this.state.data
        var sum   = 0;

        if(this.isArray(datas)){
            var dingList = datas.map((data,index) => {
                sum = sum + data.price
            })
        }

        if(sum > 0){
            return (
                <div id="sum" className="clear" style={{paddingBottom: '100px'}}>
                    <div className="col-xs-12 col-md-offset-8 col-md-4">
                        總價：
                        <span>
                            { this.intFormat(sum) }
                        </span>
                    </div>
                </div>
            )
        }

        return false
    }


    fetchOrder()
    {
        var token = this.props.token

        this.ajax('GET', `/order_list/${token}`, {}, (response) => {
            if(isset(response.success) && this.isArray(response.data)){
                this.setState({ orderList: response.data })
            }

            if(isset(response.error)){
                this.setState({ orderList: [] })
            }
        }, (err) => {
            log(err)
        })
    }


    fetchMyOrder()
    {
        var token = this.props.token

        this.ajax('GET', `/my_order/${token}`, {}, (response) => {
            if(isset(response.success) && this.isArray(response.data)){
                this.setState({ myOrder: response.data })
            }

            if(isset(response.error)){
                this.setState({ myOrder: [] })
            }
        }, (err) => {
            log(err)
        })
    }


    createOrder(e)
    {
        var bottom  = e.target;
        var menu    = this.state.menu
        var subject = this.state.subject
        var noMenu  = this.state.noMenu
        var config  = { headers: { 'content-type': 'multipart/form-data' } }
        var form    = new FormData()

        bottom.disabled = true
        bottom.innerHTML = '發送中....'

        if(!noMenu) form.append('menu', menu)
        form.append('subject', subject)
        form.append('no_menu', (noMenu) ? '1' : '0')

        this.ajax('POST', '/ding_order', form, (response) => {
            switch(true){
                case (isset(response.error)):
                    alert(response.error)
                    break
                case (isset(response.success)):
                    this.pushState([{foo: 'order'},'',response.data.link], this.setState({order: response.data, menu: {}, subject: '', noMenu: false, page: 'form'}))
                    break
            }

            bottom.disabled = false
            bottom.innerHTML = '開始訂購！'
        },(err) => {
            log(err)
        },config)
    }


    doDing(e)
    {
        var bottom = e.target;

        var data = {
            id          : this.state.id,
            name        : this.state.name,
            content     : this.state.content,
            price       : this.state.price,
            order_token : this.state.order.order_token
        }

        bottom.disabled = true
        bottom.innerHTML = '發送中....'

        this.ajax('POST', '/ding', data, (response) => {
            switch(true){
                case (isset(response.error)):
                    alert(response.error)
                    break
                case (isset(response.success)):
                    alert(response.success)
                    
                    this.setState({
                        id      : response.data.id,
                        name    : response.data.name,
                        content : response.data.content,
                        price   : response.data.price
                    })

                    break
            }

            bottom.disabled = false
            bottom.innerHTML = '我 Ding！'

        }, (err) => {
            log(err)
        })
    }


    getDing()
    {
        var datas = this.state.data
        var token = this.state.order.order_token

        if(!isset(token)) return false

        this.ajax('POST', `/ding_list/${token}`, {}, (response) => {

            if(JSON.stringify(datas) == JSON.stringify(response.data)) return false;

            if(this.isArray(response.data)){
                this.setState({data: response.data})
            }else{
                this.setState({data: []})
            }

        }, (err) => {
            log(err)
        })
    }


    dingDel(token)
    {
        if(confirm('確定取消訂購？')){
            this.ajax('DELETE', '/ding_del', {token: token, o_id: this.state.order.id}, (response) => {
                if(isset(response.message)) alert(response.message)
            }, (err) => {
                log(err)
            })
        }
    }


    copyUrl()
    {
        var linkField = document.getElementById('link')

        linkField.select()

        document.execCommand('copy')

        alert('複製成功')
    }


    pay(id)
    {
        var datas   = this.state.data
        var pointer = false;

        datas.map((data,index) => {
            if(data.id == id) pointer = index
        })

        if(pointer === false){
            log('Can\'t find Ding to assign haspay record !');
            return;
        }

        this.ajax('POST', '/haspay', {id: id}, (response) => {
            if(response.success) log(response.success)
            if(response.error) log(response.error)
        }, (err) => {
            log(err)
        })
    }


    pushState(params=[],callback = () => {})
    {
        history.pushState(params[0], params[1], params[2])
        if(typeof callback == 'function'){
            callback()
            if(params[2] == '/') this.fetchOrder()
        }
    }


    componentWillMount()
    {
        this.fetchOrder()
        this.fetchMyOrder()
    }

    componentDidMount()
    {
        var loop = () => {
            this.getDing()
            setTimeout(loop, 1000)
        }

        loop()
    }

    /*
    shouldComponentUpdate()
    {

    }
    */

    componentDidUpdate()
    {

    }

    componentWillUnmount()
    {

    }

    render() {
        return (
            <div id="wrapper" className="floatBgBefore">
                <header className="container floatBgBefore">
                    <div className="row flex flex-wrap flex-bottom">
                        <div className="col-xs-12 col-md-4 flex flex-middle">
                            <small className="fa fa-bell"></small>
                            <h1 onClick={() => this.pushState([{foo: 'index'},'','/'], this.setState({page: 'index'}))}>Ding！</h1>
                        </div>
                        <nav className="col-xs-12 col-md-8 btn-group flex flex-middle flex-right flex-wrap">
                            <button className="btn btn-default" onClick={() => this.setState({page: 'order'})}>發起新 Ding！</button>
                            <button
                                className="btn btn-default" 
                                onClick={() => this.pushState([{foo: 'order'},'',this.state.order.link], this.setState({page: 'form'}))}
                                style={{ display: (this.isObject(this.state.order) && Object.keys(this.state.order).length > 0) ? 'block' : 'none' }}
                            >
                                {`回到訂單 (${this.state.order.subject})`}
                            </button>
                            <button className="btn btn-default" onClick={() => this.setState({page: 'history'})}>我的 Ding！</button>
                        </nav>
                    </div>
                </header>
                <main className="container">

                    { this.dingPage() }
                    
                </main>
                <footer>
                    <small>
                        <span>&copy; 2017 PotsonHumer </span>
                        <a href="mailto:potsonhumer@gmail.com"><i className="fa fa-envelope"></i>potsonhumer@gmail.com</a>
                    </small>
                </footer>
            </div>
        )
    }
}